package controller

import (
	"context"
	"gitee.com/zhouxiaozhu/go-delayqueue"
	"gitee.com/zhouxiaozhu/go-delayqueue/client/cmd/grpc/pb/delay"
	"google.golang.org/grpc/codes"
	"time"
)

// DelayServer ...
type DelayServer struct {
}

// Push ...
func (server *DelayServer) Push(ctx context.Context, in *delay.PushReq) (*delay.PushResp, error) {
	msgId, err := delayqueue.Push(ctx, in.Topic, in.Body, time.Duration(in.DelayTime)*time.Second)
	if err != nil {
		return nil, err
	}
	return &delay.PushResp{MsgId: msgId}, nil
}

// Delete ...
func (server *DelayServer) Delete(ctx context.Context, in *delay.DeleteReq) (*delay.DeleteResp, error) {
	err := delayqueue.Delete(ctx, in.MsgId)
	if err != nil {
		return nil, err
	}
	return &delay.DeleteResp{Code: uint32(codes.OK), Message: codes.OK.String()}, nil
}
