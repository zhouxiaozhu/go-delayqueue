package v1

import (
	"gitee.com/zhouxiaozhu/go-delayqueue"
	"gitee.com/zhouxiaozhu/go-delayqueue/client/tools/request"
	"github.com/gin-gonic/gin"
	"time"
)

// PushReq ...
type PushReq struct {
	Topic     string      `json:"topic" binding:"required"`
	Body      interface{} `json:"body" binding:"required"`
	DelayTime int64       `json:"delay_time" binding:"required"`
}

// PushResp ...
type PushResp struct {
	MsgId string `json:"msg_id"`
}

// DeleteReq ...
type DeleteReq struct {
	MsgId string `json:"msg_id" binding:"required"`
}

// Push ...
func Push(c *gin.Context) {
	var pushReq PushReq
	if err := c.ShouldBind(&pushReq); err != nil {
		request.ParamError(c)
		return
	}

	msgId, err := delayqueue.Push(c, pushReq.Topic, pushReq.Body, time.Duration(pushReq.DelayTime)*time.Second)
	if err != nil {
		request.ServerError(c, err)
		return
	}

	request.Success(c, &PushResp{
		MsgId: msgId,
	})
}

// Delete ...
func Delete(c *gin.Context) {
	var deleteReq DeleteReq
	if err := c.ShouldBind(&deleteReq); err != nil {
		request.ParamError(c)
		return
	}

	err := delayqueue.Delete(c, deleteReq.MsgId)
	if err != nil {
		request.ServerError(c, err)
		return
	}

	request.Success(c)
}
