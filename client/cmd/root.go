package cmd

import (
	"fmt"
	"gitee.com/zhouxiaozhu/go-delayqueue/client/cmd/grpc"
	"gitee.com/zhouxiaozhu/go-delayqueue/client/cmd/http"
	"github.com/spf13/cobra"
	"os"
)

func init() {
	rootCmd.AddCommand(
		http.Cmd,
		grpc.Cmd,
	)
}

// rootCmd ...
var rootCmd = &cobra.Command{
	Use:   "go delay",
	Short: "go delay",
	Long:  `go delay`,
}

// Execute ...
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
