package initializers

import "gitee.com/zhouxiaozhu/go-delayqueue/client/initializers/redis"

func Init() {
	redis.Init()
}
