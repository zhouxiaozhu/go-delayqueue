package redis

import (
	"gitee.com/zhouxiaozhu/go-delayqueue"
	"github.com/go-redis/redis"
	"os"
)

func Init() {
	addr := os.Getenv("REDIS_HOST")
	if addr == "" {
		addr = "127.0.0.1:6379"
	}
	delayqueue.InitClient(&redis.Options{
		Addr: addr,
	})
}
