package delayqueue

import (
	"github.com/go-redis/redis"
)

var rdb redis.Cmdable

// InitClient ...
func InitClient(opt *redis.Options) {
	rdb = redis.NewClient(opt)
	if _, err := rdb.Ping().Result(); err != nil {
		panic(err)
	}
}

// InitClusterClient ...
func InitClusterClient(opt *redis.ClusterOptions) {
	rdb = redis.NewClusterClient(opt)
	if _, err := rdb.Ping().Result(); err != nil {
		panic(err)
	}
}
