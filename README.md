# go-delayqueue
基于redis zset 实现延迟队列
* http服务查看队列详情
* http/grpc客户端支持
* 集群支持
* 支持批量消费消息 TODO
* 重试机制 TODO
* ACK确认机制 TODO

### 单机版
```
delayqueue.InitClient(&redis.Options{
    Addr: "127.0.0.1:6379",
})
```

### 集群版
```
delayqueue.InitClusterClient(&redis.ClusterOptions{
    Addrs: []string{"127.0.0.1:6379"},
})
```

### 消费者
```
package main

import (
	"fmt"
	"gitee.com/zhouxiaozhu/go-delayqueue"
	"github.com/go-redis/redis"
)

func myJob(msg interface{}) {
	fmt.Println(msg)
	delayqueue.RetryEnqueue("myJob", msg)
}

func main() {
	go delayqueue.StatsServer(8000)

	delayqueue.InitClusterClient(&redis.ClusterOptions{
		Addrs: []string{"127.0.0.1:6379"},
	})
	delayqueue.Process("myJob", myJob, 3)
	delayqueue.Run()
}
```

### 生产者
```
package main

import (
	"gitee.com/zhouxiaozhu/go-delayqueue"
	"github.com/go-redis/redis"
	"time"
)

func main() {
	delayqueue.InitClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	delayqueue.Push("myJob", map[string]string{
		"content": "hello world",
	}, 10*time.Second)
}
```

### 客户端调用
##### http
```
go run client/main.go http
curl -H "Content-Type: application/json" -X POST -d '{"topic":"myJob","body":{"order_id":"d69cf3d0-3bdd-1a04-9468-aaf6dace1e97"},"delay_time":60}' 'http://localhost:5000/api/v1/delay/push'
curl -H "Content-Type: application/json" -X POST -d '{"msg_id":"717da1b8-9916-5327-ed27-e3364120c7f0"}' 'http://localhost:5000/api/v1/delay/delete'
```

##### grpc
```
go run client/main.go grpc
grpcurl -plaintext -d '{"topic":"myJob","body":{"order_id":"d69cf3d0-3bdd-1a04-9468-aaf6dace1e97"},"delay_time":60}' localhost:5000 delay.Delay.Push
grpcurl -plaintext -d '{"msg_id":"4d76d22f-405b-1ccd-20db-ec3e6e9336d1"}' localhost:5000 delay.Delay.Delete
```