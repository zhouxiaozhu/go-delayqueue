package delayqueue

import (
	"sync"
)

// Params ...
type Params struct {
	PollSize int64
}

var managers = make(map[string]*manager)
var mutex sync.Mutex

// Process ...
func Process(topic string, delayFunc delayFunc, concurrency int, params ...Params) {
	mutex.Lock()
	defer mutex.Unlock()

	manager := &manager{
		topic:       topic,
		job:         delayFunc,
		concurrency: concurrency,
	}
	if len(params) > 0 {
		manager.pollSize = params[0].PollSize
	}
	managers[topic] = manager
}

// Run ...
func Run() {
	for _, manager := range managers {
		for i := 0; i < manager.concurrency; i++ {
			go manager.consumer()
		}
	}
	select {}
}
