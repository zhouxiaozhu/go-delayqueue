package main

import (
	"fmt"
	"gitee.com/zhouxiaozhu/go-delayqueue"
	"github.com/go-redis/redis"
)

func myJob(msg interface{}) {
	fmt.Println(msg)
}

func main() {
	delayqueue.InitClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	delayqueue.Process("myJob", myJob, 3)
	delayqueue.Run()
}
